<!DOCTYPE html>
<html lang="en" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>@yield('title','Authentication')</title>

    <!-- Prevent the demo from appearing in search engines (REMOVE THIS) -->
    <meta name="robots" content="noindex">

    <!-- Perfect Scrollbar -->
    <link type="text/css" href="{{asset("$public/assets/vendor/perfect-scrollbar.css")}}" rel="stylesheet">

    <!-- Material Design Icons -->
    <link type="text/css" href="{{asset("$public/assets/css/material-icons.css")}}" rel="stylesheet">
    <link type="text/css" href="{{asset("$public/assets/css/material-icons.rtl.css")}}" rel="stylesheet">

    <!-- Font Awesome Icons -->
    <link type="text/css" href="{{asset("$public/assets/css/fontawesome.css")}}" rel="stylesheet">
    <link type="text/css" href="{{asset("$public/assets/css/fontawesome.rtl.css")}}" rel="stylesheet">

    <!-- App CSS -->
    <link type="text/css" href="{{asset("$public/assets/css/app.css")}}" rel="stylesheet">
    <link type="text/css" href="{{asset("$public/assets/css/app.rtl.css")}}" rel="stylesheet">


</head>

<body class="login">


<div class="d-flex align-items-center" style="min-height: 100vh">
    <div class="col-sm-8 col-md-6 col-lg-4 mx-auto" style="min-width: 300px;">
        <div class="text-center mt-5 mb-1">
            <div class="avatar avatar-lg">
                <img src="{{asset("$public/assets/images/logo/primary.svg")}}" class="avatar-img rounded-circle"
                     alt="OpenAcademi"/>
            </div>
        </div>
        <div class="d-flex justify-content-center mb-5 navbar-light">
            <!-- Brand -->
            <a href="student-dashboard.html" class="navbar-brand m-0">
                OpenAcademi
            </a>
        </div>
        <div class="card navbar-shadow">
            @yield('content')
        </div>
    </div>
</div>


<!-- jQuery -->
<script src="{{asset("$public/assets/vendor/jquery.min.js")}}"></script>

<!-- Bootstrap -->
<script src="{{asset("$public/assets/vendor/popper.min.js")}}"></script>
<script src="{{asset("$public/assets/vendor/bootstrap.min.js")}}"></script>

<!-- Perfect Scrollbar -->
<script src="{{asset("$public/assets/vendor/perfect-scrollbar.min.js")}}"></script>

<!-- MDK -->
<script src="{{asset("$public/assets/vendor/dom-factory.js")}}"></script>
<script src="{{asset("$public/assets/vendor/material-design-kit.js")}}"></script>

<!-- App JS -->
<script src="{{asset("$public/assets/js/app.js")}}"></script>

<!-- Highlight.js -->
<script src="{{asset("$public/assets/js/hljs.js")}}"></script>

<!-- App Settings (safe to remove) -->
<script src="{{asset("$public/assets/js/app-settings.js")}}"></script>


</body>

</html>